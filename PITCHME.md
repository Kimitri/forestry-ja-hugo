# Forestry ja Hugo

---

## Miksi?

+++

## Miksi ei Drupal 8?

- D8 vaatii tuekseen hyvin monimutkaista infrastruktuuria
- D8-kehitystyö on monimutkaista
- Sivustot vaativat jatkuvaa ylläpitoa
- Sivustojen suorituskyky on huono
- Monissa tapauksissa alustan jousto on näennäistä

+++

## Miksi staattinen sivusto?

- Sivustot eivät aseta hostingille juuri mitään vaatimuksia
- Sivustot eivät aseta kehitysympäristöille juuri mitään vaatimuksia
- Sivustot voivat olla kokonaan versiohallittuja
- Sivustojen tallentaminen välimuistiin on täysin triviaalia

+++

## Miksi _oikeasti_ staattinen sivusto?

- Staattiset sivustot ovat:
  - kevyitä
  - nopeita
  - edullisia
  - turvallisia
- Staattiset sivustot eivät edes tarvitse palvelinta

---

## Mikä Hugo on?

- [Hugo](https://gohugo.io/) on ns. _Static Site Generator_
- Hugo luo kokonaisen staattisen sivuston kasasta sisältötiedostoja
- Hugo on _todella_ nopea

---

## Mikä Forestry on?

- Forestry on _sisällönhallintajärjestelmä_
- Forestryllä hallitaan _vain sisältöä_ - Forestry ei ota ollenkaan kantaa sivuston ulkoasuun
- Kaikki sisältömuutokset Forestryssä tallentuvat Git-versiohallintaan - kaikki on aina versioitua
- Forestry on kaupallinen tuote, joka asuu osoitteessa [Forestry.io](https://forestry.io)

---

## Työskentely Forestryn ja Hugon kanssa

+++

## Päivittäjän työskentely Forestryn kanssa

- [Live-demo](https://forestry.io)

+++

## Kehittäjän työskentely Forestryn ja Hugon kanssa

- Tietomallia hallitaan Forestryssä
- Hugo tarjoaa valmiin, erinomaisen hyvän ja nopean paikallisen kehitysympäristön
- Kaikki sisältö synkronoidaan aina versiohallinnan kautta - tiedostoja ja tietokantadumppeja ei tarvitse siirrellä

---

## Miten tieto liikkuu?

- [Kaavio](https://www.plectica.com/maps/NW69NK1IW)